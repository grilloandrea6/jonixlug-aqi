/*
JonixLUG-AQI V1 Air Quality Indicator
Advanced dust sensor (PM2.5 and PM10) with fine tuning temperature and humidity algorithm.
July 2019
License: GPLv3
Contacts: https://gitlab.com/JonixLUG jonixlug@gmail.com
  Dario P. & Vincenzo Q. (Team JonixLUG)
Partners: Piersoft https://www.piersoft.it/ Peacelink https://www.peacelink.it/ Andrea Grillo grilloandrea6@gmail.com
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
This code is intended to be used on a nodeMCU Lolin board.  
in combination with:
- DHT22 (temperature and humidity) https://learn.adafruit.com/dht
- SDS011 (PM2.5 and PM10 dust sensor) by Nova Fitness Co.,Ltd http://www.inovafitness.com
and some libraries included in the project's files:
- Adafruit DHT Humidity & Temperature Sensor https://travis-ci.org/adafruit/DHT-sensor-library.svg?branch=master
- Adafruit Unified Sensor Driver https://github.com/adafruit/Adafruit_Sensor
- SDS011 Arduino library By R. Zschiegner reviewed (sds011.cpp) by Piersoft https://gitlab.com/JonixLUG/jonixlug-aqi/SDS011-ricki-z-Piersoft
- RunningAverage https://github.com/RobTillaart/Arduino/tree/master/libraries/RunningAverage
- base64_arduino Base64 encoder/decoder https://travis-ci.org/Densaugeo/base64_arduino.svg?branch=master
Compile it with Arduino IDE 1.8.9 or later https://www.arduino.cc/en/Main/Software#download
Data will be sent to ThingSpeak's server, so it need a free or paid account. https://thingspeak.com
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
~ Warning: This sketch will not work "as is". It needs to be modified adding your personal ThingSpeak's data. ~
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
# First of all, setup the software environment, installing Arduino IDE 1.8.9 or later and adding this URL:
  https://arduino.esp8266.com/stable/package_esp8266com_index.json
  in "File -> Preferences -> Additional Boards Manager URLs" field.
# Move "libraries" folder to Arduino IDE's main libraries folder location (eg. ~/Arduino/libraries for GNU/Linux)
# Change in this sketch (ThingSpeak Settings) lines: "ThingSpeak channel" and "upload API key", with your personal account data.
# Make a WiFi access point with SSID "jonixlug-aqi" and (the same) "jonixlug-aqi" as WPA-PSK password, or better,
change (in Wi-Fi Settings) "ssid" and "pass" variables with yours.
# Next steps are wiring sensors to Wemos mini D1's digital inputs.
DHT22 need to be connected to 3,3V Vcc, Ground and VDD serial data pin
SDS011 need 5Vcc, Ground, TX and RX pins
Wemos mini D1 can be programmed and powered by onboard mini USB port.
Warning: we need 2 Ground pins for both DHT22 and SDS011 sensors, but Wemos mini D1 has only 1 Ground Pin.
Don't panic, it can be used simultaneously for all sensors. Simply wire it together.
Please visit https://gitlab.com/JonixLUG/jonixlug-aqi for schemes and infos about wiring and power your Advanced dust sensor.
# Finally verify and if there're not errors, upload sketch on Wemos mini D1 Board.
Tips: Open a "monitor" on Arduino IDE and read debug infos to make sure things are going well. 
Data will be sent to ThingSpeak server and can be visualized as graph by Matlab and reused as json, XML and CSV format.
Power source can be anything with miniUSB 5Vdc @ at least 500mA output (better 1A). 
*/

/**************************
 * INCLUDES               *
 **************************/

#include <DHT.h>
#include <SDS011.h>
#include "base64.hpp"
#include "RunningAverage.h"
#include <WiFiClientSecure.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiType.h>
#include <ESP8266WiFiAP.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiSTA.h>
#include <WiFiUdp.h>
#include <ESP8266mDNS.h>


/**************************
 * SETTINGS               *
 **************************/

// Settings you MUST customise

const char* ssid = "Piersoft"; // your wireless network name (SSID)

const char* pass = "12345678"; // your Wi-Fi network password

String writeAPIKey = "YYYYYYYYY123456"; // your ThingSpeak channel write API key


// Settings you may want to customise

#define pinDHT D7 // pin for DHT22

#define pinSDS_RX D1 // pin for SDS011 TX

#define pinSDS_TX D2 // pin for SDS011 RX

const int sleepTime = 15; // interval in minutes between readings


// Settings you should not need to customise

const char* server = "184.106.153.149"; // ThingSpeak server IP

const int httpPort = 80; // ThingSpeak server port

#define DHTTYPE DHT22 // type of DHT sensor

#define DEBUG // debug level


/**************************
 * DECLARATIONS           *
 **************************/

// SDS011
SDS011 sds;

// DHT22
DHT dht(pinDHT, DHTTYPE);

// Wi-Fi client
WiFiClient client;

int error;
float p10, p25;
RunningAverage pm25Stats(10);
RunningAverage pm10Stats(10);


/**************************
 * FUNCTIONS              *
 **************************/


/**************************
 * Setup                  *
 **************************/
void setup() {
  
  // Set Serial speed (set the same in the monitor)
  Serial.begin(9600);
  Serial.println("");
  
  // init SDS011
  sds.begin(pinSDS_RX, pinSDS_TX);
  delay(1000);
}


/**************************
 * Loop                   *
 **************************/
void loop() {
  // wake SDS011 up
  sds.wakeup();
  
   // reset vars
  pm25Stats.clear();
  pm10Stats.clear();
  
  // connect to wi-fi network
  wifiConnect();
  
  
  // be patient with SDS011
  Serial.println("Calibrating SDS011 (20 sec)");
  delay(20000);
  
  // read 10 values and make average
  int i=0;
  while(i<10)
  {
    error = sds.read(&p25, &p10);
    if (!error && p25 < 999 && p10 < 1999) {
      pm25Stats.addValue(p25);
      pm10Stats.addValue(p10);
      i++;
    } 
    delay(1500);
  }
  Serial.println("Average PM10: " + String(pm10Stats.getAverage()) + ", PM2.5: "+ String(pm25Stats.getAverage()));

  // init DHT22
  dht.begin();
  // be patient with DHT
  delay(1000);

  // Humidity
  float h = dht.readHumidity();
  
  // Temperature
  float t = dht.readTemperature();
  
  // Check values
  if (isnan(h) || isnan(t)) {
    Serial.println("No data from DHT!");
  } else {
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.println("°C");
    Serial.print("Humidity: ");
    Serial.print(h);
    Serial.println("%\t");
    Serial.flush();
    
    // Humidity correction for PM 2.5 and PM 10
    float pm25n=normalizePM25(float(pm25Stats.getAverage()),h);
    float pm10n=normalizePM10(float(pm10Stats.getAverage()),h);
    Serial.print("Normalized PM10: ");
    Serial.println(pm10n);
    Serial.print("Normalized PM2.5: ");
    Serial.println(pm25n);
    
    // Send data to ThingsSpeak
    WiFiClient client;
    if (!client.connect(server, httpPort)) {
      Serial.println("Connection to ThingSpeak failed");
      client.stop();
      return;
    }  
    Serial.println("Connected to ThingSpeak server");
    String url = "GET /update?api_key="+writeAPIKey+"&field1=" + String(pm10n) + "&field2=" + String(pm25n) + "&field3=" + String(t) + "&field4=" + String(h) + " HTTP/1.1\r\n" + "Host: " + server + "\r\n" + "Connection: close\r\n" + "\r\n";
    Serial.print("Requesting URL: ");
    Serial.println(url);
    client.print(url);
    // show server response
    while (client.connected() || client.available()) {
      if (client.available()) {
        String line = client.readStringUntil('\n');
        Serial.println(line);
      }
    }
    client.stop();
  }
  delay(3000);
  
  // Sleep until next run
  Serial.println("Sleep " + String(sleepTime) + " minutes");
  sds.sleep();
  delay(sleepTime * 60 * 1000);
}



/*************************************
 * Humidity correction               *
 * by Zbyszek Kiliański & Piotr Paul *
 *************************************/
float normalizePM25(float pm25, float humidity){
  return pm25/(1.0+0.48756*pow((humidity/100.0), 8.60068));
}
float normalizePM10(float pm10, float humidity){
  return pm10/(1.0+0.81559*pow((humidity/100.0), 5.83411));
}


/****************************
 * Connect to Wi-Fi network *
 ****************************/
void wifiConnect() {
  Serial.println("Connecting to Wi-fi network ");

  WiFi.mode(WIFI_STA);
  Serial.println(ssid);
  
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED) {
      delay(250);
      Serial.print(".");
    }
  }
  Serial.println("");
  Serial.println("Connected!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
