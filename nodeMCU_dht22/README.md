# NodeMCU v3 Lolin + Nova PM Sensor SDS011 + DHT22

This directory contains the code for the NodeMCU Lolin board

Follow the [environment setup instructions](../README.md#software-environment-setup)

Select the board `NodeMCU 1.0 (ESP-12E Module)`

Edit the code and customise your settings:

- Wi-fi ssid
- Wi-fi password
- ThingSpeak write API key

Compile and upload the sketch to the NodeMCU board

## Wiring diagram

Based on [Luftdaten](https://luftdaten.info/en/construction-manual/)

![NodeMCU - SDS011 - DHT22](nodeMCU_dht22_wiring.jpg)