# Lolin D1 mini + DHT22

This directory contains the code for the NodeMCU Lolin board and just a DHT22 temperature / humidity sensor

Follow the [environment setup instructions](../README.md#software-environment-setup)

Edit the code and customise your settings:

- Wi-fi ssid
- Wi-fi password
- ThingSpeak write API key

Compile and upload the sketch to the board

Use pin D2 for DHT22 data cable